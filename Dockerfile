FROM gcr.io/google.com/cloudsdktool/google-cloud-cli:alpine
RUN apk add --update-cache age kustomize

RUN gcloud components install kubectl gke-gcloud-auth-plugin

RUN wget -qcO - https://raw.githubusercontent.com/viaduct-ai/kustomize-sops/master/scripts/install-ksops-archive.sh | bash

